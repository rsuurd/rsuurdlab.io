import {createApp} from 'vue'
import App from './App.vue'
import VueGtag from "vue-gtag-next";
import {createI18n} from "vue-i18n";

import '@picocss/pico/css/pico.min.css'

createApp(App)
    .use(VueGtag, {
        property: {
            id: "G-C0NBFL5QSF",
            params: {
                send_page_view: true
            }
        }
    })
    .use(createI18n({
        locale: navigator.language.substr(0, 2),
        fallbackLocale: 'en',
        messages: {
            en: {
                description: 'With over 15 years of experience in IT, I can help with software development, quality & delivery. My goal: maintainable software that makes the user happy.',
                motto: 'Better, more often.',
                contact: {
                    address: 'Address',
                    phone: 'Phone number',
                    email: 'Email',
                    kvk: 'KVK number',
                }
            },
            nl: {
                description: 'Met meer dan 15 jaar ervaring in de ICT kan ik helpen op het gebied van software development, quality & delivery. Mijn doel is onderhoudbare software waar de gebruiker blij van wordt.',
                motto: 'Betere software, vaker.',
                contact: {
                    address: 'Adres',
                    phone: 'Telefoonnummer',
                    email: 'E-mail',
                    kvk: 'KVK-nummer',
                }
            }
        }
    }))
    .mount('#app')
